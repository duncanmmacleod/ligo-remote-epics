# -*- coding: utf-8 -*-
# Copyright (C) 2023 Cardiff University

"""Launch MEDM using EPICS tunnelled through SSH.
"""

import argparse
import getpass
import logging
import os
import signal
import shlex
import subprocess
import sys
from logging.handlers import TimedRotatingFileHandler
from pathlib import Path
from shutil import which

import sshtunnel

try:
    from coloredlogs import (
        ColoredFormatter,
        terminal_supports_colors,
    )
except ImportError:
    def terminal_supports_colors(stream):  # cannot use colours
        return False


__author__ = "Duncan Macleod <duncan.macleod@ligo.org>"
__credits__ = "Jonathan Hanks <jonathan.hanks@ligo.org>"
__version__ = "1.0.0"

#: default username for SSH connections
DEFAULT_USERNAME = os.getenv("LIGO_USER", getpass.getuser())

#: dict of remote EPICS host names for each observatory
EPICS_REMOTE_HOST = {
    "H": "lhoepics.ligo-wa.caltech.edu",
    "L": "lloepics.ligo-la.caltech.edu",
}

#: global property recording child process executed via subprocess
CHILD_PROCS = []


def init_logging(name, level=logging.INFO, stream=sys.stderr):
    """Return the logger with the given name, or create one as needed.

    If the logger as no handlers attached already, a new
    `logging.Handler` will be created and attached based on ``stream``.

    Parameters
    ----------
    name : `str`
        The name of the logger.

    level : `int`, `str`
        The level to set on the logger.

    stream : `io.IOBase`, `str`
        The stream to write log messages to.
        If a `str` is given as either ``"stdout"``, ``"stderr"`` logs
        will be written to those default streams, any other `str` is
        interpreted as a file path in which to maintain a
        `logging.TimedRotatingFileHandler` with daily rotation and
        three days of backup.

    Returns
    -------
    logger : `logging.Logger`
        A new, or existing, `Logger` instance, with at least one
        `~logging.Handler` attached.
    """
    # stream to console
    if stream in ("stdout", "stderr"):
        stream = getattr(sys, stream)

    logger = logging.getLogger(name)
    logger.propagate = False
    logger.setLevel(level)
    if not logger.handlers:
        if isinstance(stream, (str, Path)):
            # create the parent directory
            Path(stream).parent.mkdir(parents=True, exist_ok=True)
            # create the rotating file handler
            handler = TimedRotatingFileHandler(
                str(stream),
                when="d",
                interval=1,
                backupCount=3,
            )
            # if logging to file, never use colour
            formatter_class = logging.Formatter
        else:
            handler = logging.StreamHandler(stream)
            if terminal_supports_colors(stream):
                formatter_class = ColoredFormatter
            else:
                formatter_class = logging.Formatter
        handler.setFormatter(formatter_class(
            '%(asctime)s | %(name)s | %(levelname)+8s | %(message)s',
        ))
        logger.addHandler(handler)
    return logger


def default_macros(obs):
    """Return the default `dict` of macros for a given observatory.

    Parameters
    ----------
    obs : `str`
        Single-character identifier for an observatory,
        e.g. ``"H"`` or ``"L"``.

    Returns
    -------
    macros : `dict`
        A `dict` of macros that can be passed to ``medm``.

    Notes
    -----
    The `USERAPPS` macros is taken from the environment variable of the
    same name, defaulting to ``"/opt/rtcds/userapps/release"``.

    Example
    -------
    >>> default_macros("H")
    {'IFO': 'H1',
     'ifo': 'h1',
     'SITE': 'LHO',
     'site': 'lho',
     'USERAPPS': '/opt/rtcds/userapps/release'}
    """
    obs = obs[0]
    ifo = f"{obs}1"
    site = f"L{obs}O"
    return {
        "IFO": ifo,
        "ifo": ifo.lower(),
        "SITE": site,
        "site": site.lower(),
        "USERAPPS": os.getenv("USERAPPS", "/opt/rtcds/userapps/release"),
    }


def get_password(user):
    """Prompt for and return the password for the given ``user``.
    """
    return getpass.getpass(prompt=f"Password for {user}: ")


def _get_screen(screen, obs, macros):
    """Return the path of a screen.

    This just handles the following special cases:

    - ``"sitemap"`` - returns the path of the sitemap.adl file
      for the given ``obs``

    - ``"ODC"`` - returns ``"SYS_CUST_ODC_MASTER_WORD.adl"``

    All other values for ``screen`` are returned unmodified.
    """
    if screen == "sitemap":
        return f"/ligo/cds/l{obs.lower()}o/medm/sitemap.adl"
    if screen == "ODC":
        macros.setdefault("ODCMASTER", "MASTER")
        userapps = macros.get("USERAPPS", "")
        return f"{userapps}/sys/common/medm/SYS_CUST_ODC_MASTER_WORD.adl"
    return screen


def parse_ssh_auth_options(
    username,
    password=None,
    ssh_pkey=None,
    ssh_private_key_password=None,
    **kwargs,
):
    """Configure the auth options for SSH.

    This function goes through some logic to prompt for passwords/passphrases
    as necessary.
    """
    auth_options = dict(kwargs)
    auth_options.update({
        "ssh_username": username,
        "ssh_pkey": ssh_pkey,
    })

    # if ssh key was not given, prompt for basic auth password
    if ssh_pkey is None:
        auth_options["ssh_password"] = getpass.getpass(
            prompt=f"Password for {username}: ",
        )
    # otherwise if the passphrase for the key was not given
    # and the key cannot be opened without one, prompt for one
    elif (
        not ssh_private_key_password
        and not sshtunnel.SSHTunnelForwarder.read_private_key_file(
            pkey_file=ssh_pkey,
            pkey_password=None,
            logger=None,
        )
    ):
        auth_options["ssh_private_key_password"] = getpass.getpass(
            prompt=f"Passphrase for {ssh_pkey}: ",
        )

    # if given a password or told to use a key...
    if auth_options.get("ssh_pkey") or auth_options.get("ssh_password"):
        # disable ssh agent
        auth_options.setdefault("allow_agent", False)
        # and disable key discovery
        auth_options.setdefault("host_pkey_directories", [])

    return auth_options


def handle_signal(signum, frame):
    """Propagate a signal to all child processes if registered.
    """
    for proc in CHILD_PROCS:
        proc.send_signal(signum)


for _sig in (
    signal.SIGINT,
    signal.SIGTERM,
):
    signal.signal(_sig, handle_signal)


def remote_medm(
    obs,
    screen="sitemap",
    medm=which("medm") or "medm",
    macros=None,
    **kwargs,
):
    """Execute ``medm` with remote EPICS configuration for the given observatory.

    Parameters
    ----------
    obs : `str`
        Single-character identifier for an observatory,
        e.g. ``"H"`` or ``"L"``.

    screen : `str`
        The screen to load, either ``"sitemap"`` or ``"ODC"`` or the file name
        of the MEDM screen to open.

    medm : `str`
        The path of the MEDM executable to run.

    macros : `dict`
        Key/value pairs defining macros to pass to ``medm`` via the ``-macro``
        argument.

    remote_epics_kwargs
        All other keywords are passed to the `remote_epics_command` function.
    """
    if macros is None:
        macros = {}
    macros = default_macros(obs) | macros
    screen = _get_screen(screen, obs, macros)

    cmd = [
        medm,
        "-macro",
        ",".join((f"{k}={v}" for k, v in macros.items())),
        "-x",
        screen,
    ]

    return remote_epics_command(
        obs,
        cmd,
        env={k: macros[k] for k in ("IFO", "SITE")},
        **kwargs,
    )


def remote_epics_command(
    obs,
    command,
    env=None,
    username=DEFAULT_USERNAME,
    password=None,
    ssh_pkey=None,
    ssh_private_key_password=None,
    remote_bind_address="192.168.25.2",
    remote_port=5064,
    local_bind_address="localhost",
    local_port=5000,
    logger=None,
):
    """Execute a command on top of an SSH-tunneled remote EPICS configuration
    for the given observatory.

    Parameters
    ----------
    obs : `str`
        Single-character identifier for an observatory,
        e.g. ``"H"`` or ``"L"``.

    command : `list` of `str`
        Command to run.

    env : `dict`
        Extra environment variables to set in the subprocess environment.

    username : `str`
        The name of the user to connect as.

    password : `str`
        The password to use for ``username`` when connecting over SSH.

    ssh_pkey : `str`
        Path to SSH private key to use when connecting.

    ssh_private_key_password : `str`
        Passphrase for ``ssh_pkey``.

    remote_bind_address : `str`
        The path of the remote address to use for port forwarding.

    remote_bind_port : `int`
        The port on the remote host to forward.

    local_bind_address : `str`
        The path of the local address to use for port forwarding.

    local_port : `int`
        The local port to bind.

    logger : `logging.Logger`
        The logger to use, otherwise use a default logger.
    """
    if logger is None:
        logger = init_logging("ligo-remote-epics")

    host = EPICS_REMOTE_HOST[obs]
    site = "L{obs[0].upper()}O"

    if isinstance(command, str):
        command = shlex.split(command)

    auth_options = parse_ssh_auth_options(
        username,
        password=password,
        ssh_pkey=ssh_pkey,
        ssh_private_key_password=ssh_private_key_password,
    )

    logger.info(f"Opening SSH tunnel to {host}")
    with sshtunnel.open_tunnel(
        # remote host
        ssh_address_or_host=host,
        # binding options
        remote_bind_address=(remote_bind_address, remote_port),
        local_bind_address=(local_bind_address, local_port),
        logger=logger,
        # auth options
        **auth_options,
    ) as tunnel:
        logger.info("SSH tunnel open, launching medm")

        logger.debug(f"$ {shlex.join(command)}")

        # manually use Popen and 'with ...' to handle more signals
        cur = subprocess.Popen(
            command,
            env=(
                os.environ
                | {
                    "EPICS_CA_AUTO_ADDR_LIST": "NO",
                    "EPICS_CA_ADDR_LIST": "",
                    "EPICS_CA_NAME_SERVERS": f"localhost:{local_port}",
                    "EPICS_REMOTE_PATH": f"https://{host}",
                    "LIGO_EPICS_REMOTE_CONNECTION": site,
                }
                | (env or {})
            ),
        )
        CHILD_PROCS.append(cur)
        with cur as proc:
            try:
                proc.communicate()  # wait forever
            except Exception:
                proc.kill()  # kill the child
                raise
            retcode = proc.poll()
        logger.info("Closing down...")
        CHILD_PROCS.remove(cur)
        logger.debug(f"{command[0]} exited with code {retcode}")
    return retcode


class RemoteEpicsArgumentParser(argparse.ArgumentParser):
    """`argparse.ArgumentParser` with some extra defaults.
    """
    def __init__(self, *args, **kwargs):
        kwargs.setdefault("description", __doc__)
        kwargs.setdefault(
            "formatter_class",
            argparse.ArgumentDefaultsHelpFormatter,
        )
        super().__init__(*args, **kwargs)


def split_macro(arg):
    """Split a `--macro` option value on the first `=` character.
    """
    return arg.split("=", 1)


def create_remote_medm_parser(obs=None, **kwargs):
    """Create a `RemoteEpicsArgumentParser` for command-line argument parsing.
    """
    parser = RemoteEpicsArgumentParser(**kwargs)

    if obs is None:
        parser.add_argument(
            "observatory",
            choices=["H", "L"],
            help="Observatory to view",
        )
    parser.add_argument(
        "-s",
        "--screen",
        default="sitemap",
        help=(
            "Name of screen to open. Either 'sitemap', 'ODC' or the "
            "path/name of an ADL file"
        ),
    )
    parser.add_argument(
        "-u",
        "--user",
        default=DEFAULT_USERNAME,
        help="Username to use when authenticating with remote EPICS host.",
    )
    parser.add_argument(
        "-i",
        "--identity-file",
        default=None,
        help=(
            "Path to SSH private key file to use with SSH."
        ),
    )
    parser.add_argument(
        "-d",
        "--debug",
        default=False,
        action="store_true",
        help="Output debug logging information",
    )
    parser.add_argument(
        "-m",
        "--macro",
        action="append",
        dest="macros",
        metavar="KEY=VALUE",
        type=split_macro,
        help=(
            "Additional macro to pass (verbatim) to medm, "
            "can be given multiple times"
        ),
    )

    return parser


def main_remote_medm(args=None, obs=None, **parser_kwargs):
    """Run `remote_medm` from the command-line.
    """
    parser = create_remote_medm_parser(obs=obs, **parser_kwargs)
    opts = parser.parse_args(args=args)
    logger = init_logging(
        "ligo-remote-medm",
        level=logging.DEBUG if opts.debug else logging.INFO,
    )
    remote_medm(
        obs or opts.observatory,
        screen=opts.screen,
        macros=dict(opts.macros or []),
        logger=logger,
        username=opts.user,
        ssh_pkey=opts.identity_file,
    )


def main_remote_medm_lho(*args, **kwargs):
    """Launch remote MEDM for LIGO-Hanford using EPICS tunnelled through SSH.
    """
    return main_remote_medm(
        *args,
        obs="H",
        description=main_remote_medm_lho.__doc__,
        **kwargs,
    )


def main_remote_medm_llo(*args, **kwargs):
    """Launch remote MEDM for LIGO-Livingston using EPICS tunnelled through SSH.
    """
    return main_remote_medm(
        *args,
        obs="L",
        description=main_remote_medm_llo.__doc__,
        **kwargs,
    )


def create_remote_epics_parser(**kwargs):
    """Create a `RemoteEpicsArgumentParser` for command-line argument parsing.
    """
    parser = RemoteEpicsArgumentParser(**kwargs)

    parser.add_argument(
        "observatory",
        choices=["H", "L"],
        help="Observatory to view",
    )
    parser.add_argument(
        "command",
        nargs="+",
        default=which("bash"),
        help="Command to execute",
    )
    parser.add_argument(
        "-u",
        "--user",
        default=DEFAULT_USERNAME,
        help="Username to use when authenticating with remote EPICS host.",
    )
    parser.add_argument(
        "-i",
        "--identity-file",
        default=None,
        help=(
            "Path to SSH private key file to use with SSH."
        ),
    )
    parser.add_argument(
        "-d",
        "--debug",
        default=False,
        action="store_true",
        help="Output debug logging information",
    )

    return parser


def main_remote_epics(args=None):
    """Execute a command on top of a remote EPICS SSH tunnel.
    """
    parser = create_remote_epics_parser(description=main_remote_epics.__doc__)
    opts = parser.parse_args(args=args)
    logger = init_logging(
        "ligo-remote-epics",
        level=logging.DEBUG if opts.debug else logging.INFO,
    )
    remote_epics_command(
        opts.observatory,
        command=opts.command,
        logger=logger,
        username=opts.user,
        ssh_pkey=opts.identity_file,
    )


if __name__ == "__main__":
    sys.exit(main_remote_medm())
